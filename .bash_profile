if [ -f ~/.bashrc ]; then . ~/.bashrc; fi

# Setting PATH for Python 2.7
# The orginal version is saved in .bash_profile.pysave
PATH="/Applications/NAMD_2.10b1_MacOSX-x86_64-multicore:/Library/Frameworks/Python.framework/Versions/2.7/bin:${PATH}"
export PATH

export HuangLab="/Users/handuo/Documents/Research/HuangLab/"
export Course="/Users/handuo/Documents/Courses/"
